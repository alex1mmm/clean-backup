package ut.ru.matveev.alexey.plugins.jira.cleanbackup;

import org.junit.Test;
import ru.matveev.alexey.plugins.jira.cleanbackup.api.MyPluginComponent;
import ru.matveev.alexey.plugins.jira.cleanbackup.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}