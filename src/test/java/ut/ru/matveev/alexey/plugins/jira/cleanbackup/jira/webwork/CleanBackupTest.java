package ut.ru.matveev.alexey.plugins.jira.cleanbackup.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.matveev.alexey.plugins.jira.cleanbackup.jira.webwork.CleanBackup;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class CleanBackupTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CleanBackup testClass = new CleanBackup();

        throw new Exception("CleanBackup has no tests!");

    }

}
