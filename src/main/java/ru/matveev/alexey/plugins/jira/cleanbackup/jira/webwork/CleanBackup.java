package ru.matveev.alexey.plugins.jira.cleanbackup.jira.webwork;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.inject.Inject;
import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Scanned
public class CleanBackup extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(CleanBackup.class);
    private String backupName;
    private String tablePrefix;
    private final String homePath;
    private final String importPath;
    private final String unzipFolder;

    @Inject
    public CleanBackup(@ComponentImport JiraHome jiraHome) {
        homePath = jiraHome.getHomePath();
        importPath = homePath + "/import/";
        unzipFolder = importPath + "unzipTest/";
    }

    @Override
    public String execute() throws Exception {
        return super.execute(); //returns SUCCESS
    }

    public String getBackupname() {
        return backupName;
    }

    public void setBackupname(String backupName) {
        this.backupName = backupName;
    }

    public String getTableprefix() {
        return tablePrefix;
    }

    public void setTableprefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }


    private void unzipFile(String backupName) throws IOException {
        File dir = new File(unzipFolder);
        dir.mkdir();
        String fileZip = importPath + backupName;
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            String fileName = zipEntry.getName();
            File newFile = new File(unzipFolder + fileName);
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    private void zipFiles(String fileName) throws IOException {
        FileUtils.deleteQuietly(new File(importPath + fileName));
        List<String> srcFiles = Arrays.asList(unzipFolder + "entities.xml", unzipFolder + "activeobjects.xml");
        FileOutputStream fos = new FileOutputStream(importPath + fileName);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for (String srcFile : srcFiles) {
            File fileToZip = new File(srcFile);
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        fos.close();
        FileUtils.deleteDirectory(new File(unzipFolder));
    }

    public String doClean() throws IOException, SAXException, TransformerException {
        unzipFile(getBackupname());
        TableFilter xr = new TableFilter(XMLReaderFactory.createXMLReader(), getTableprefix());
        Source src = new SAXSource(xr, new InputSource(unzipFolder + "activeobjects.xml"));
        Result res = new StreamResult(unzipFolder + "activeobjects_.xml");
        TransformerFactory.newInstance().newTransformer().transform(src, res);
        (new File(unzipFolder + "activeobjects_.xml")).renameTo(new File(unzipFolder + "activeobjects.xml"));
        zipFiles(getBackupname());
        return SUCCESS;

    }
}
