package ru.matveev.alexey.plugins.jira.cleanbackup.jira.webwork;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;


public class TableFilter extends XMLFilterImpl {
    private boolean skip;
    private boolean skipCr;
    private final String tablePrefix;

    public TableFilter(XMLReader xmlReader, String tablePrefix) {
        super(xmlReader);
        this.tablePrefix = tablePrefix;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts)
            throws SAXException {
        if (isAOUnusedStart(qName, atts)) {
            skip = true;
            skipCr = true;
        } else {
            if (!skip) {
                super.startElement(uri, localName, qName, atts);
                skip = false;
                skipCr = false;
            }
        }

    }



    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (skip && isAOUnusedClosed(qName)) {
            skip = false;
            skipCr = true;
        } else if (!skip) {
            super.endElement(uri, localName, qName);
        }
    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (!skipCr) {
            super.characters(ch, start, length);
        }
    }

    private boolean isAOUnusedStart(String qName, Attributes atts) {
        if (qName.equals("table") && atts.getValue("name").contains(tablePrefix)) {
            return true;
        }
        if (qName.equals("data") && atts.getValue("tableName").contains(tablePrefix)) {
            return true;
        }

        return false;
    }

    private boolean isAOUnusedClosed(String qName) {
        if (qName.equals("table")) {
            return true;
        }
        if (qName.equalsIgnoreCase("data")) {
            return true;
        }
        return false;
    }
}

